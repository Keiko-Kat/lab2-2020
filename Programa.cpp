#include <string>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "Pilas.cpp"
using namespace std;

int main(){
	//Se crea el objeto tipo Pilas para correr el programa//
	Pilas *pila = new Pilas();
	//Se llama a la funcion run de Pilas//
	//run() contiene las instrucciones necesarias para funcionar//
	pila->run();
	//Se retorna 0 para acabar el main//
	
	return 0;
}
