#include <string>
#include <stdlib.h>
#include <iostream>
#include <math.h>
using namespace std;


#ifndef _PILAS_H_
#define _PILAS_H_

class Pilas{
	//Primero se define la clase Pilas//
	private:	
	//Luego se definen las caracteristicas privadas de Persona//
	//Las que se inicializan con valores neutros//
		int tope = 0;
		bool band;
		int max = 0;
	public:
	//se crean dos constructores//
	//Uno por defecto y uno que recibe tres parametros// 
		Pilas();
		Pilas(int tope, bool band, int max);
		
	//Se crean los metodos definidos por los algoritmos entregados en el laboratorio 2//
		void pila_vacia(int tope, bool &band);
		
		void pila_llena(int tope, int max, bool &band);
		
		void push(int pila[], int &tope, int max, int dato);
		
		void pop(int pila[], int &tope);
		
	//Se definen los metodos set, que reciben parametros ingresados por el usuario//
		void set_tope(int tope);
		void set_band(bool band);
		void set_max(int max);
			
	//Y se definen metodos get, que retornan los parametros pertenecientes al objeto tipo Pilas//
		int get_tope();
		int get_max();
		bool get_band();
	//catch_string se asegura de evitar un error de conversion//
		string catch_string(string in);
		
	//print_pila se encarga de imprimir los contenidos de la pila creada//
		void print_pila(int pila[], int tope);
		
	//max_in y push_in se encargan de recibir los ingresos de teclado para inicializar max e ingresar elementos a la pila respectivamente//
		int max_in();
		int push_int();
		
	//menu contiene las opciones que se le entregan al usuario para correr el programa//
		int menu();
	//run contiene todas las funciones requeridas para hacer funcionar el programa//
		void run();
};
#endif
