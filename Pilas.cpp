#include <string>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "Pilas.h"
using namespace std;

Pilas::Pilas(){}

//El contructor Pilas recibe tres parametros entregados por el usuario//
Pilas::Pilas(int tope, bool band, int max){
	//Y estos parametros se insertan en las variables correspondientes//
	this->tope = tope;
	this->band = band;
	this->max = max;
}
//pila_vacia revisa si la pila posee objetos en su interior//
void Pilas::pila_vacia(int tope, bool &band){
	//Esto se realiza revisando el valor de tope//
	//si tope es igual a 0 band se torna true//
	if (tope == 0){
		band = true;
	}else{
		//si tope es mayor que 0 band se torna false//
		band = false;
	}
}
//pila_llena revisa si queda espacio que llenar en la pila//
void Pilas::pila_llena(int tope, int max, bool &band){
	//Esto se realiza revisando el valor de tope//
	//si tope es igual a max band se torna true//
	if(tope == max){
		band = true;
	}else{
		//si tope es menor que max band se torna false//
		band = false;
	}
}
//push inserta un dato ingresado por el usuario en la pila//
void Pilas::push(int pila[], int &tope, int max, int dato){
	//Primero se llama a pila_llena para comprobar que haya espacio//
	pila_llena(tope, max, this->band);
	//Si no hay espacio, se muestra un mensaje de error//
	if(this->band == true){
		cout<<"Desbordamiento, Pila llena"<<endl;
	}else{
		//Si hay espacio se inserta el dato en la posicion tope de pila[]//
		pila[tope] = dato;
		//Y se suma 1 a tope//
		tope++;
	}
}
//pop quita el ultimo dato ingresado a la pila//
void Pilas::pop(int pila[], int &tope){
	//Primero se llama a pila_llena para comprobar que haya elementos en la pila//
	pila_vacia(tope, this->band);
	//Si no hay elementos se muestra un mensaje de error//
	if(this->band == true){
		cout<<"Subdesbordamiento, Pila vacía"<<endl;
	}else{
		//Si existen elementos el marcaor tope disminuye en 1//		
		tope--;
	}
}

//El metodo set de tope recibe un int entregado por el usuario//
void Pilas::set_tope(int tope){
	//El cual se inserta en la caracteristica tope del objeto Pilas//
	this->tope = tope;
}
//El metodo set de band recibe un bool entregado por el usuario//
void Pilas::set_band(bool band){
	//El cual se inserta en la caracteristica band del objeto Pilas//
	this->band = band;
}
//El metodo set de max recibe un int entregado por el usuario//
void Pilas::set_max(int max){
	//El cual se inserta en la caracteristica max del objeto Pilas//
	this->max = max;
}
//Los metodos get, extraen la informacion almacenada en el objeto Pilas//
//Para que el usuario pueda accesarlos//
int Pilas::get_tope(){
	return this->tope;
}
int Pilas::get_max(){
	return this->max;
}
bool Pilas::get_band(){
	return this->band;
}

//catch_string se asegura de evitar un error de conversion//
//Asegurando que solo existan numeros en el string ingresado//
string Pilas::catch_string(string in){
	//Esta funcion recibe un string ingresado por teclado en la funcion principal//
	//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
	bool is_true = true;
	//Se abre un ciclo while que corre mientras is_true sea verdadero//
	while (is_true == true){
		//Dentro del while se abre un ciclo for que analiza el string recibido//
		for(int j = 0; j < in.size(); j++){
			//Se recorre y se comparan los valores ASCII contenidos en el string//
			//A los valores ASCII de los numros del 0-9 y al valor de '-'//
			if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
				//si el contenido del string coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
				is_true = false;
			}else{
				//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
				is_true = true;
				break;
			}
		}
		//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
		if(is_true == true){
			//Se entrega un mensaje de error//
			cout<<"Por favor ingrese valores numéricos:"<<endl;
			//Y se pide otro ingreso//
			getline(cin, in);
		}
	}
	//De esta forma se asegura que la funcion 'stoi()' no entregue error//
	return in;
}

//print_pila muestra por pantalla el contenid de la pila//
void Pilas::print_pila(int pila[], int tope){
	//Se imprime una linea de guiones para mostrar una separacion//
	cout<<"-------------"<<endl;
	//Se abre un ciclo for que recorre el array desde el ultimo elemento hasta el primero//
	for(int i = tope-1; i >= 0; i--){
		//Se imprime cada dato dentro de pila//
		cout<<"    ["<<pila[i]<<"]"<<endl;
	}
	//Y finalmente se imprime una linea de guiones para mostrar una separacion//
	cout<<"-------------"<<endl;
}

//La funcion menu se utiliza para decidir que hacer en el programa//
int Pilas::menu(){
	//Se define una variable string que recibe un ingreso de teclado//
	//Y una variable int que recibe el valor y lo retorna a la funcion principal//
	string in;
	int temp;
	cout<<endl;
	//Luego se imprimen las opciones que se le dan al usuario en pantalla//
	cout<<"Agregar/Push           [1]"<<endl;
	cout<<"Eliminar/Pop           [2]"<<endl;
	cout<<"Ver Pila               [3]"<<endl;
	cout<<"Cerrar programa        [0]"<<endl;
	cout<<"--------------------------"<<endl;
	cout<<"--------------------------"<<endl;
	cout<<"Opción:  ";
	//Para luego pedir que ingrese la opcion que desea realizar//
	//Se recibe como ingreso de teclado//
	getline(cin, in);
	//Y se comprueba que sea un valor completamene numerico//
	in = catch_string(in);
	//Para luego transformarlo a una variable int//		
	temp = stoi(in);
	//Y retornar dicho int como eleccion a la funcion principal//
	return temp;
}

//max_in recibe del usuario el dato max y comprueba que sea un int//
int Pilas::max_in(){
	//Se define una variable string que recibe un ingreso de teclado//
	//Y una variable int que recibe el valor y lo retorna a la funcion principal//
	string in;
	int out;
	//Luego se le pide al usuario ingresar el maximo//
	cout<<"Ingrese el tamaño maximo de la pila:"<<endl;	
	//Se recibe como ingreso de teclado//
	getline(cin, in);
	//Y se comprueba que sea un valor completamene numerico//
	in = catch_string(in);
	//Se transforma en un int//
	out = stoi(in);
	//Y se retorna a la funcion principal//
	return out;
}
//push_int recibe un dato del usuario y comprueba que sea un int//
int Pilas::push_int(){
	//Se define una variable string que recibe un ingreso de teclado//
	//Y una variable int que recibe el valor y lo retorna a la funcion principal//
	string in;
	int out;
	//Luego se le pide al usuario que ingrese un numero//
	cout<<"Ingrese el numero que desea insertar:"<<endl;	
	//Se recibe como ingreso de teclado//
	getline(cin, in);
	//Y se comprueba que sea un valor completamene numerico//
	in = catch_string(in);
	//Se transforma en un int//
	out = stoi(in);
	//Y se retorna a la funcion principal//
	return out;
}
//run contiene todas las instrucciones para correr el programa//
void Pilas::run(){
	//Se crea el int temp, para mantener el programa corriendo//
	int temp = -1;
	//Y se crean tres ints mas para guardar las informaciones necesarias//
	int tope = 0, max, data;
	//Y una variable bool que permitira definir los pasos posibles al llenar y vaciar la pila//
	bool band = true;
	//Primero se llama a max_in para recibir el dato max//
	max = max_in();
	//Y todos los datos necesarios se insertan en sus respectivos metodos setter//	
	set_band(band);
	set_max(max);
	set_tope(tope);
	//Se crea la pila como un array de tipo int y tamaño max// 
	int pila[this->max];
	//Y se abre un while que correra mientras temp sea distinto a 0//
	while (temp != 0){
		//Luego se llama a menu para decidir que hara el programa//
		temp = menu();		
		//Y en base a temp se abre una condicional switch//
		switch(temp){
			//En caso de que temp sea 1, se llama a push int para recibir un dato del usuario//
			case 1: data = push_int();
					//Y se inserta en la pila llamando a push//
					push(pila, this->tope, this->max, data); 
					break;
			//En caso de que temp sea 2, se llama a pop para eliminar el ultimo dato ingresado//
			case 2: pop(pila, this->tope);
					break;
			//En caso de que temp sea 3, se llama a print_pilla para imprimir los contenidos de pila//
			case 3: print_pila(pila, this->tope);
					break;
			//En caso de que temp sea 0, el programa se salta el switch y se cierra el ciclo while//
			case 0: 
					break;
			//En caso de que temp no sea ninguna de las 4 opciones anteriores se envia un mensaje de error//
			default: cout<<"opcion no valida, intente de nuevo"<<endl;
					//Y se reinicia el while//
					break;	
		}	
	}
}
